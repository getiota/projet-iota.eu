---
title: Politique de protection des données personnelles
disclaimer-en: Our privacy policy is provided in French only. A rough translation is available using <a>Google Translate</a>. Please contact us if you have any question.
---


## 1. Généralités

Iota Banking SAS, société par actions simplifiée au capital de 15 000 euros, immatriculée au registre du commerce et des sociétés de Toulouse sous le numéro 882 831 720 dont le siège social est situé 19 rue des Jeux Floraux, 31130 Balma, France (« nous ») invite les utilisateurs (« vous ») à lire attentivement la présente Politique de protection des données personnelles avant d’utiliser le site Internet www.projet-iota.eu (le « Site »). Nous accordons une grande importance à la protection de votre vie privée.

La Politique de protection des données personnelles précise la manière dont nous gérons les traitements de données à caractère personnel liés à l’utilisation du Site, en notre qualité de Responsable de Traitement et en application des dispositions du Règlement Général sur la Protection des Données (Règlement (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016) et de la loi du 20 juin 2018 relative à la protection des données portant modification de la loi « Informatique et Libertés » du 6 janvier 1978. La Politique de protection des données personnelles peut être modifiée à tout moment, aussi nous vous conseillons de la consulter régulièrement.


## 2. Collecte d’informations personnelles

Les informations que vous communiquez par le biais des formulaires disponibles sur le site sont destinées aux personnels habilités de notre société, à des fins de gestion administrative et commerciale.

Pour pouvoir répondre à vos questions ou à vos demandes, vous envoyer une lettre d’information périodique, nos communiqués de presse, vous adresser des publications et vous permettre de prendre rendez-vous avec nous pour un appel téléphonique, nous pouvons être amenés à vous demander, à titre facultatif ou obligatoire, des informations personnelles telles que votre adresse de courrier électronique, votre nom, votre prénom ou votre numéro de téléphone mobile.

Ces informations :
-   seront traitées loyalement et licitement ;
-   seront enregistrées pour des finalités déterminées et légitimes ;
-   seront utilisées conformément à ces finalités ;
-   sont adéquates, pertinentes et non excessives par rapport à ces finalités ;
-   feront l’objet de précautions de nature à assurer la sécurité et la confidentialité des données en vue d’empêcher qu’elles puissent être endommagées, modifiées, détruites ou communiquées à des tiers non autorisés.


## 3. Finalités de la collecte

La collecte a pour finalité :
-   de vous adresser nos publications à votre demande ;
-   de répondre à vos questions et vos demandes d’information ;
-   et de manière plus générale, de permettre le fonctionnement du Site.


## 4. Destinataires de la collecte

Les données personnelles collectées au moyen des formulaires de contact sont destinées aux personnes en charge du traitement de l’information au sein de notre société.

Les données personnelles que nous collectons sur le Site sont à usage purement interne et ne font l’objet d’aucune communication, cession ou divulgation à des tiers, sans l’autorisation expresse et écrite de l’utilisateur concerné, sauf contraintes légales ou judiciaires.


## 5. Durée de conservation des informations personnelles

Les données personnelles et/ou informations relatives à votre navigation sur le Site ne seront pas conservées au-delà de la durée nécessaire aux finalités du traitement concerné.

Pour ce qui concerne les données de contact, la durée de conservation est de deux ans après le dernier contact avec l’utilisateur concerné si celui-ci y consent.


## 6. Droit d’accès, de rectification et de suppression

Conformément à la réglementation applicable, vous disposez de différents droits, à savoir :
-   Droit d’accès : vous pouvez obtenir des informations concernant le traitement de vos données personnelles ainsi qu’une copie de ces données personnelles ;
-   Droit de rectification : si vous estimez que vos données personnelles sont inexactes ou incomplètes, vous pouvez exiger que ces données personnelles soient modifiées en conséquence ;
-   Droit à l’effacement : vous pouvez exiger l’effacement de vos données personnelles dans la limite de ce qui est permis par la réglementation ;
-   Droit à la limitation du traitement : vous pouvez demander la limitation de traitement de vos données personnelles ;
-   Droit d’opposition : vous pouvez vous opposer au traitement de vos données personnelles, pour des motifs liés à une situation particulière. Vous disposez du droit absolu de vous opposer au traitement de vos données personnelles à des fins de prospection commerciale, y compris le profilage lié à cette prospection ;
-   Droit à la portabilité des données : quand ce droit est applicable, vous avez le droit que les données personnelles que vous nous avez fournies vous soient rendues ou, lorsque cela est possible, techniquement, de les transférer à un tiers ;
-   Droit de définir des directives relatives à la conservation, l’effacement ou la communication de vos données personnelles, applicables après votre décès ;
-   Droit de retirer votre consentement : si vous avez donné votre consentement au traitement de vos données personnelles, vous avez le droit de retirer votre consentement à tout moment.

Ces droits peuvent être exercés sur simple demande écrite, impérativement accompagnée d’une copie d’un justificatif d’identité en cours de validité comportant la signature du titulaire, envoyée à la société Iota Banking, 19 rue des Jeux Floraux 31130 Balma ou par courrier électronique à l’adresse : legal@projet-iota.eu


## 7. Sécurité

Nous vous informons prendre toutes précautions utiles, mesures organisationnelles et techniques appropriées pour préserver la sécurité, l'intégrité et la confidentialité de vos données à caractère personnel et notamment, empêcher qu'elles soient déformées, endommagées ou que des tiers non autorisés y aient accès.


## 8. Modifications

Nous nous réservons le droit de modifier notre Politique de protection des données personnelles à tout moment et sans préavis. Les modifications apportées à la Politique de protection des données personnelles entreront en vigueur une fois publiées sur le Site.


## 9. Réclamation

Pour toute question concernant notre Politique de protection des données personnelles, vous pouvez effectuer votre demande par email à l’adresse suivante : legal@projet-iota.eu

Conformément à la réglementation applicable, vous êtes également en droit d’introduire une réclamation auprès de la Commission Nationale de l’Informatique et des Libertés (CNIL).
