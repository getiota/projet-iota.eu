---
title: Legal Notice
---

## Company

Iota Banking SAS, with a capital of 15 000 euros, registered with the RCS (Company Registry) of Toulouse, number 882 831 720.

## Registered Headquarters

19 rue des Jeux Floraux, 31130 Balma, France.

## Contact

contact@projet-iota.eu

## Publishers

Nicolas Miart, CEO (Président)

Guillaume Aquilina, CTO (Directeur Général)

## Hosting Provider

The projet-iota.eu website is hosted with Google Firebase within the EU in Frankfurt (Germany), as mandated by General Data Protection Regulation 2016/679.