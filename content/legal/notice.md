---
title: Mentions légales
---

## Éditeur

Iota Banking SAS, société par actions simplifiée au capital de 15 000 euros, immatriculée au RCS de Toulouse sous le numéro 882 831 720.

## Siège social

19 rue des Jeux Floraux, 31130 Balma, France.

## Contact

contact@projet-iota.eu

## Directeurs de publication

Nicolas Miart, Président

Guillaume Aquilina, Directeur Général

## Prestataire d’hébergement

Le site www.projet-iota.eu est hébergé chez Google Firebase, à Francfort (Allemagne) sur le territoire de l'Union Européenne conformément aux dispositions du Règlement Général sur la Protection des Données (n° 2016/679).