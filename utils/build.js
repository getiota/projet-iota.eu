const glob = require('glob')

function localizeRoutes(routes, locales) {
  return locales.flatMap((l) => {
    return routes.map((r) => `${l}/${r}`)
  })
}

function getContentPaths(subfolder) {
  const files = glob.sync(`${subfolder}/*.md`, { cwd: 'content' })
  const routes = files.map((d) => d.substr(0, d.lastIndexOf('.')))
  return routes
}

function getContentRoutes(subfolder, locales) {
  const routes = getContentPaths(subfolder)
  return routes.concat(localizeRoutes(routes, locales))
}

module.exports = { getContentRoutes }
