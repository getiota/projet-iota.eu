const rewire = require('rewire')
const test = require('ava')
const utils = rewire('../../utils/index.js')

const localizeRoutes = utils.__get__('localizeRoutes')

test('Localize routes', (t) => {
  t.deepEqual(localizeRoutes(['legal/notice', 'index'], ['it', 'en']), [
    'it/legal/notice',
    'it/index',
    'en/legal/notice',
    'en/index'
  ])
})
