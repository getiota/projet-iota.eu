/*
 ** TailwindCSS Configuration File
 **
 ** Docs: https://tailwindcss.com/docs/configuration
 ** Default: https://github.com/tailwindcss/tailwindcss/blob/master/stubs/defaultConfig.stub.js
 */
module.exports = {
  theme: {
    colors: {
      // primary
      pri: {
        lighter: '#9ef3eb',
        light: '#00DBC8',
        pure: '#00B5A8',
        dark: '#097975',
        white: '#f3fcfb'
      },
      // secondary
      sec: {
        dark: '#8E1132',
        light: '#ffd9e0',
        white: '#F9EFF1',
        whiter: '#faf2f4'
      },
      // neutral
      neu: {
        white: '#F9F9F9',
        dark: '#666666',
        darker: '#1D1D1D'
      },
      white: '#ffffff'
    },
    fontFamily: {
      display: ['Vollkorn', 'serif'],
      body: ['Source Sans Pro', 'sans-serif']
    },
    container: {
      padding: '2rem'
    },
    fontSize: {
      xs: '.75rem',
      sm: '.875rem',
      base: '1rem',
      lg: '1.125rem',
      xl: '1.25rem',
      '2xl': '1.5rem',
      '3xl': '1.875rem',
      '4xl': '2.25rem',
      '5xl': '3rem',
      '6xl': '4rem',
      '10xl': '7.5rem'
    },
    zIndex: {
      '-10': '-10'
    }
  },
  variants: {},
  plugins: []
}
