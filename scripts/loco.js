const path = require('path')
const fs = require('fs')
const axios = require('axios')

const key = process.env.LOCALISE_KEY

const locales = ['fr', 'en']

locales.forEach((lang) => {
  const url = `https://localise.biz/api/export/locale/${lang}.json?key=${key}&fallback=${locales[0]}&printf=icu`
  axios
    .get(url)
    .then(function({ data }) {
      const langPath = path.resolve(__dirname, `../lang/${lang}.json`)
      fs.writeFile(langPath, JSON.stringify(data, null, 2), () => {})
    })
    .catch(() => {})
})
