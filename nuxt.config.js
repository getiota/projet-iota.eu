import path from 'path'
import Mode from 'frontmatter-markdown-loader/mode'
import { getContentRoutes } from './utils/build.js'

require('dotenv').config()

const locales = [
  { code: 'fr', iso: 'fr-FR', file: 'fr.json' },
  { code: 'en', iso: 'en-US', file: 'en.json' }
]

export default {
  // mode: 'spa',
  head: {
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: process.env.npm_package_description || ''
      }
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }]
  },
  loading: { color: '#00DBC8' },
  css: [],
  vue: {
    config: {
      devtools: true
    }
  },
  generate: {
    routes: getContentRoutes(
      'legal',
      locales.slice(1).map((l) => {
        return l.code
      })
    )
  },
  env: {
    baseUrl: process.env.BASE_URL || 'https://projet-iota.eu',
    firebaseUrl: process.env.FIREBASE_URL
  },
  buildModules: [
    '@nuxtjs/eslint-module',
    '@nuxtjs/tailwindcss',
    '@nuxtjs/dotenv'
  ],
  modules: [
    'nuxt-helmet',
    '@nuxtjs/axios',
    [
      'nuxt-i18n',
      {
        locales,
        defaultLocale: locales[0].code,
        lazy: true,
        langDir: 'lang/',
        baseUrl: process.env.BASE_URL
      }
    ]
  ],
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {},
  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {
      if (ctx.isDev) {
        config.devtool = ctx.isClient ? 'source-map' : 'inline-source-map'
      }
      // add frontmatter-markdown-loader
      config.module.rules.push({
        test: /\.md$/,
        include: path.resolve(__dirname, 'content'),
        loader: 'frontmatter-markdown-loader',
        options: {
          mode: [Mode.VUE_COMPONENT, Mode.META]
        }
      })
    }
  }
}
